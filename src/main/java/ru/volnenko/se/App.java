package ru.volnenko.se;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.volnenko.se.config.ApplicationConfig;
import ru.volnenko.se.controller.Bootstrap;
import ru.volnenko.se.config.BootstrapConfig;

public class App {

    public static void main(String[] args) throws Exception {
        final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(BootstrapConfig.class);
        context.register(ApplicationConfig.class);
        context.refresh();

        final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.start();
    }

}
