package ru.volnenko.se.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import ru.volnenko.se.api.service.CommandProvider;
import ru.volnenko.se.command.AbstractCommand;

import java.util.ArrayList;
import java.util.Map;

@Configuration
@ComponentScan("ru.volnenko.se")
public class BootstrapConfig {

    @Bean
    public CommandProvider commandProvider(@Lazy Map<String, AbstractCommand> commands) {
        return () -> new ArrayList(commands.values());
    }
}
