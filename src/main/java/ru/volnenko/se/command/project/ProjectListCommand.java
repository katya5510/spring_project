package ru.volnenko.se.command.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.entity.Project;

/**
 * @author Denis Volnenko
 */
@Component(ProjectListCommand.PROJECT_LIST)
public final class ProjectListCommand extends AbstractCommand {

    public static final String PROJECT_LIST = "project-list";

    @Autowired
    private IProjectService projectService;

    @Override
    public String command() {
        return PROJECT_LIST;
    }

    @Override
    public String description() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        for (Project project : projectService.getListProject()) {
            System.out.println("id:" + project.getId() + "  |  name:" + project.getName());
        }
        System.out.println();
    }

}
