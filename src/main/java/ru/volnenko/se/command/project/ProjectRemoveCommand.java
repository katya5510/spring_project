package ru.volnenko.se.command.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.api.service.ITerminalService;
import ru.volnenko.se.command.AbstractCommand;

/**
 * @author Denis Volnenko
 */
@Component(ProjectRemoveCommand.PROJECT_REMOVE)
public final class ProjectRemoveCommand extends AbstractCommand {

    public static final String PROJECT_REMOVE = "project-remove";

    @Autowired
    private ITerminalService terminalService;

    @Autowired
    private IProjectService projectService;

    @Override
    public String command() {
        return PROJECT_REMOVE;
    }

    @Override
    public String description() {
        return "Remove selected project.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVING PROJECT]");
        System.out.println("Enter project id:");
        final Integer id = terminalService.nextInteger();
        if (id == null) {
            System.out.println("Error! Incorrect id...");
            System.out.println();
            return;
        }
        projectService.removeProjectById(id);
        System.out.println("[OK]");
    }

}
