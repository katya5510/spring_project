package ru.volnenko.se.command.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.command.AbstractCommand;

/**
 * @author Denis Volnenko
 */
@Component(ProjectClearCommand.PROJECT_CLEAR)
public final class ProjectClearCommand extends AbstractCommand {

    public static final String PROJECT_CLEAR = "project-clear";

    @Autowired
    private IProjectService projectService;

    @Override
    public String command() {
        return PROJECT_CLEAR;
    }

    @Override
    public String description() {
        return "Remove all projects.";
    }

    @Override
    public void execute() {
        projectService.clear();
        System.out.println("[ALL PROJECTS REMOVED]");
    }

}
