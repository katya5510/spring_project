package ru.volnenko.se.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.entity.Domain;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

@Component(DataJsonSaveCommand.DATA_JSON_SAVW)
public class DataJsonSaveCommand extends AbstractCommand {

    public static final String DATA_JSON_SAVW = "data-json-save";

    private IProjectService projectService;

    private ITaskService taskService;

    public DataJsonSaveCommand(final IProjectService projectService,
                               final ITaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public String description() {
        return "Save data json.";
    }

    @Override
    public String command() {
        return DATA_JSON_SAVW;
    }

    @Override
    public void execute() {
        final Domain domain = new Domain();
        domain.getProjects().addAll(projectService.getListProject());
        domain.getTasks().addAll(taskService.getListTask());

        final ObjectMapper objectMapper = new ObjectMapper();
        try (OutputStream outputStream = new FileOutputStream("data.json")) {
            objectMapper.writerWithDefaultPrettyPrinter().writeValue(outputStream, domain);
            System.out.println("OK");
        } catch (IOException e) {
            System.out.println("Exception on save json" + e.getMessage());
        }
    }
}
