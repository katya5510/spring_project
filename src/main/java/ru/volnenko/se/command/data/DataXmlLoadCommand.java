package ru.volnenko.se.command.data;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.entity.Domain;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

@Component(DataXmlLoadCommand.DATA_XML_LOAD)
public class DataXmlLoadCommand extends AbstractCommand {

    public static final String DATA_XML_LOAD = "data-xml-load";

    private IProjectService projectService;

    private ITaskService taskService;

    public DataXmlLoadCommand(final IProjectService projectService,
                               final ITaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public String description() {
        return "Load data xml.";
    }

    @Override
    public String command() {
        return DATA_XML_LOAD;
    }

    @Override
    public void execute() {
        final ObjectMapper objectMapper = new XmlMapper();
        try (InputStream inputStream = new FileInputStream("data.xml")) {
            JsonParser parser = objectMapper.reader().createParser(inputStream);
            Domain domain = parser.readValueAs(Domain.class);
            projectService.load(domain.getProjects());
            taskService.load(domain.getTasks());
            System.out.println("OK");
        } catch (IOException e) {
            System.out.println("Exception on save json" + e.getMessage());
        }
    }

}
