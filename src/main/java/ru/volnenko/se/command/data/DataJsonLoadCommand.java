package ru.volnenko.se.command.data;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.entity.Domain;

import java.io.*;

@Component(DataJsonLoadCommand.DATA_JSON_LOAD)
public class DataJsonLoadCommand extends AbstractCommand {

    public static final String DATA_JSON_LOAD = "data-json-load";

    private IProjectService projectService;

    private ITaskService taskService;

    public DataJsonLoadCommand(final IProjectService projectService,
                               final ITaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public String description() {
        return "Load data json.";
    }

    @Override
    public String command() {
        return DATA_JSON_LOAD;
    }

    @Override
    public void execute() {
        final ObjectMapper objectMapper = new ObjectMapper();
        try (InputStream inputStream = new FileInputStream("data.json")) {
            JsonParser parser = objectMapper.reader().createParser(inputStream);
            Domain domain = parser.readValueAs(Domain.class);
            projectService.load(domain.getProjects());
            taskService.load(domain.getTasks());
            System.out.println("OK");
        } catch (IOException e) {
            System.out.println("Exception on save json" + e.getMessage());
        }
    }
}
