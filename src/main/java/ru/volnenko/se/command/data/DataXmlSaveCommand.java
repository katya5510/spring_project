package ru.volnenko.se.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.entity.Domain;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

@Component(DataXmlSaveCommand.DATA_XML_SAVE)
public class DataXmlSaveCommand extends AbstractCommand {

    public static final String DATA_XML_SAVE = "data-xml-save";

    private IProjectService projectService;

    private ITaskService taskService;

    public DataXmlSaveCommand(final IProjectService projectService,
                              final ITaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public String description() {
        return "Save data xml.";
    }

    @Override
    public String command() {
        return DATA_XML_SAVE;
    }

    @Override
    public void execute() {
        final Domain domain = new Domain();
        domain.getProjects().addAll(projectService.getListProject());
        domain.getTasks().addAll(taskService.getListTask());

        final ObjectMapper objectMapper = new XmlMapper();
        try (OutputStream outputStream = new FileOutputStream("data.xml")) {
            objectMapper.writerWithDefaultPrettyPrinter().writeValue(outputStream, domain);
            System.out.println("OK");
        } catch (IOException e) {
            System.out.println("Exception on save json" + e.getMessage());
        }
    }
}
