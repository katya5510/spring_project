package ru.volnenko.se.command.system;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.ITerminalService;
import ru.volnenko.se.command.AbstractCommand;

/**
 * @author Denis Volnenko
 */
@Component(HelpCommand.HELP)
public final class HelpCommand extends AbstractCommand {

    public static final String HELP = "help";

    @Autowired
    private ITerminalService terminalService;

    @Override
    public String command() {
        return HELP;
    }

    @Override
    public String description() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        for (final AbstractCommand command : terminalService.getCommands()) {
            System.out.println(command.command() + ": " + command.description());
        }
    }

}
