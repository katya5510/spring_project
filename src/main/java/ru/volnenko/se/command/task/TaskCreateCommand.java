package ru.volnenko.se.command.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.api.service.ITerminalService;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.entity.Project;
import ru.volnenko.se.service.TaskService;

import java.util.List;

/**
 * @author Denis Volnenko
 */
@Component(TaskCreateCommand.TASK_CREATE)
public final class TaskCreateCommand extends AbstractCommand {

    public static final String TASK_CREATE = "task-create";

    @Autowired
    private ITerminalService terminalService;

    @Autowired
    private TaskService taskService;

    @Autowired
    private IProjectService projectService;

    @Override
    public String command() {
        return TASK_CREATE;
    }

    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]");

        System.out.println("[PROJECT LIST]");
        List<Project> projects = projectService.getListProject();
        if (projects.isEmpty()) {
            System.out.println("NEED TO CREATE PROJECT");
            return;
        }
        for (Project project: projects) {
            System.out.println("id:" + project.getId() + "  |  name:" + project.getName());
        }

        System.out.println("\nENTER PROJECT ID:");
        Integer projectId = getProjectId();
        if (projectId == null) {
            return;
        }

        System.out.println("ENTER NAME TASK:");
        final String name = terminalService.nextLine();
        taskService.createTaskByProject(projectId, name);
        System.out.println("[OK]");
        System.out.println();
    }

    private Integer getProjectId() {
        final Integer projectId = terminalService.nextInteger();
        if (projectId == null) {
            System.out.println("Error! Incorrect id...");
            System.out.println();
            return null;
        }
        return projectId;
    }

}
