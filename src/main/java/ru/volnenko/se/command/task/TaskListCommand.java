package ru.volnenko.se.command.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.command.AbstractCommand;
import ru.volnenko.se.entity.Task;

/**
 * @author Denis Volnenko
 */
@Component(TaskListCommand.TASK_LIST)
public final class TaskListCommand extends AbstractCommand {

    public static final String TASK_LIST = "task-list";
    @Autowired
    private ITaskService taskService;

    @Override
    public String command() {
        return TASK_LIST;
    }

    @Override
    public String description() {
        return "Show all tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        for (Task task: taskService.getListTask()) {
            System.out.println("id:" + task.getId() + "  |  name:" + task.getName() + "  |  project id:" + task.getProjectId());
        }
        System.out.println();
    }

}
