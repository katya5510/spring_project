package ru.volnenko.se.command.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.api.service.ITerminalService;
import ru.volnenko.se.command.AbstractCommand;

/**
 * @author Denis Volnenko
 */
@Component(TaskRemoveCommand.TASK_REMOVE)
public final class TaskRemoveCommand extends AbstractCommand {

    public static final String TASK_REMOVE = "task-remove";

    @Autowired
    private ITerminalService terminalService;

    @Autowired
    private ITaskService taskService;

    @Override
    public String command() {
        return TASK_REMOVE;
    }

    @Override
    public String description() {
        return "Remove selected task.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVING TASK]");
        System.out.println("Enter task id:");
        final Integer taskId = terminalService.nextInteger();
        if (taskId == null) {
            System.out.println("Error! Incorrect id...");
            System.out.println();
            return;
        }
        taskService.removeTaskById(taskId);
        System.out.println("[OK]");
    }

}
