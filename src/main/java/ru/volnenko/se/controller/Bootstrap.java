package ru.volnenko.se.controller;

import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.ITerminalService;
import ru.volnenko.se.command.AbstractCommand;

import java.util.Map;

/**
 * @author Denis Volnenko
 */
@Component
public final class Bootstrap {

    private final Map<String, AbstractCommand> commands;

    private ITerminalService terminalService;

    public Bootstrap(final Map<String, AbstractCommand> commands,
                     final ITerminalService terminalService) {
        this.terminalService = terminalService;
        this.commands = commands;
    }

    public void start() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            command = terminalService.nextLine();
            execute(command);
        }
    }

    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

}
