package ru.volnenko.se.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.volnenko.se.api.repository.ITaskRepository;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.entity.Task;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * @author Denis Volnenko
 */
@Service
public final class TaskService implements ITaskService {

    @Autowired
    private ITaskRepository taskRepository;

    @Override
    public Task createTask(final String name) {
        if (name == null || name.isEmpty()) return null;
        Task task = new Task();
        task.setName(name);
        return taskRepository.save(task);
    }

    @Override
    public Task getTaskById(final Integer id) {
        if (id == null) return null;
        return taskRepository.getOne(id);
    }

    @Override
    public Task merge(final Task task) {
        return taskRepository.save(task);
    }

    @Override
    public void removeTaskById(final Integer id) {
        taskRepository.deleteById(id);
    }

    @Override
    public List<Task> getListTask() {
        return taskRepository.findAll();
    }

    @Override
    public void clear() {
        taskRepository.deleteAll();
    }

    @Override
    public Task createTaskByProject(final Integer projectId, final String taskName) {
        Task task = new Task();
        task.setName(taskName);
        task.setProjectId(projectId);
        return taskRepository.save(task);
    }

    @Override
    public Task getByOrderIndex(Integer orderIndex) {
        List<Task> tasks = taskRepository.findAll();
        Task task = tasks.get(orderIndex);
        return task == null ? null : task;
    }

    @Override
    public void merge(Task... tasks) {
        taskRepository.saveAll(Arrays.asList(tasks));
    }

    @Override
    public void load(Task... tasks) {
        taskRepository.saveAll(Arrays.asList(tasks));
    }

    @Override
    public void load(Collection<Task> tasks) {
        taskRepository.saveAll(tasks);
    }

    @Override
    public void removeTaskByOrderIndex(Integer orderIndex) {
        List<Task> tasks = taskRepository.findAll();
        Task task = tasks.get(orderIndex);
        taskRepository.deleteById(task.getId());
    }
}
