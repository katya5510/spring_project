package ru.volnenko.se.service;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.volnenko.se.api.repository.IProjectRepository;
import ru.volnenko.se.api.repository.ITaskRepository;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.entity.Project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * @author Denis Volnenko
 */
@Service
@Transactional
public final class ProjectService implements IProjectService {

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private ITaskRepository taskRepository;

    @Override
    public Project createProject(final String name) {
        if (name == null || name.isEmpty()) return null;
        final Project project = new Project();
        project.setName(name);
        return projectRepository.save(project);
    }

    @Override
    public Project merge(final Project project) {
        if (project == null) return null;
        return projectRepository.save(project);
    }

    @Override
    public Project getProjectById(final Integer id) {
        if (id == null) return null;
        return projectRepository.getOne(id);
    }

    @Override
    public void removeProjectById(final Integer id) {
        if (id == null) return;
        taskRepository.deleteByProjectId(id);
        projectRepository.deleteById(id);
    }

    @Override
    public List<Project> getListProject() {
        java.lang.Iterable<Project> iterator = projectRepository.findAll();
        if (!iterator.iterator().hasNext()) {
            return new ArrayList<>();
        }
        return Lists.newArrayList(iterator);
    }

    @Override
    public void clear() {
        taskRepository.deleteAll();
        projectRepository.deleteAll();
    }

    @Override
    public void merge(final Project... projects) {
        if (projects == null || projects.length == 0) return;
        projectRepository.saveAll(Arrays.asList(projects));
    }

    @Override
    public void load(Collection<Project> projects) {
        if (projects == null) return;
        projectRepository.saveAll(projects);
    }

    @Override
    public void load(final Project... projects) {
        if (projects == null) return;
        projectRepository.saveAll(Arrays.asList(projects));
    }

    @Override
    public Project removeByOrderIndex(Integer orderIndex) {
        if (orderIndex == null) return null;
        List<Project> projects = projectRepository.findAll();
        Project project = projects.get(orderIndex);
        projectRepository.deleteById(project.getId());
        return project;
    }

}
