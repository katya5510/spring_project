package ru.volnenko.se.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.volnenko.se.entity.Task;

@Repository
public interface ITaskRepository extends JpaRepository<Task, Integer> {

    void deleteByProjectId(Integer projectId);
}
